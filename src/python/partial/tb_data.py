'''
This script will show you how to do some different things with non-text data.
'''
import numpy as np
import pandas as pd
import scipy as sp

from sklearn.svm import LinearSVC
from sklearn.decomposition import PCA, TruncatedSVD
from sklearn.model_selection import train_test_split
from keras.callbacks import EarlyStopping, TensorBoard

from tools import *

# Importing the data
outdir = ''
records = pd.read_csv()
full = pd.read_csv()

# Separating MTB from the rest of the columns
tb = np.array()
features = np.array()
n_features = 
n_records = 

# Splitting our data into training and test sets; stratify by TB, 
# and save 25% of the data for testing
seed = 10221983
train, test = train_test_split()

'''
Part 1: Dimension reduction
'''
# Running PCA and tranforming the raw data, keeping only the 
# top 64 principal components
pca = PCA()
pca_features = pca.fit_transform()

# Writing files to disk for visualization
metadata = records[['mtb', 'sex', 'cough', 'fever',
                    'headache', 'night_sweats']]
metadata.to_csv(outdir + 'metadata.tsv', sep='\t', index=False)
pd.DataFrame(pca_features).to_csv(outdir + 'pca_features.tsv',
                              sep='\t',
                              index=False,
                              header=None)

'''
Part 2: Using an SVM to predict TB from the 2 feature sets
'''
# Fitting an SVM
svm = LinearSVC()
svm.fit()
svm_preds = svm.predict()
svm_stats =  binary_metrics()

# And now with the PCA features
pca_svm = LinearSVC()

'''
Part 3: Using a network to do both tasks at once
'''
# Building the model and setting up some callbacks for training
nn = DNN()
nn_stop = EarlyStopping(monitor='loss',
                        patience=30)
nn_board = TensorBoard(log_dir=outdir + 'logs/',
                       write_graph=True)

# Compiling the model. Use Adam for optimization and binary cross-entropy
# for the loss.
nn.compile()

# Fitting the model to the training data. Run the procedure
# for 500 epochs, and remember to add the callbacks.
nn.fit()

# Generating and thresholding the probabilities. Use flu prevalence
# as the cutoff to convert the probabilities to class predictions.
nn_preds = nn.predict()
nn_stats = binary_metrics()

# Using grid_metrics to find the optimal threshold.
nn_gm = grid_metrics()
best_f1 = np.max(nn_gm.f1)
f1_cutoff = nn_gm.cutoff[]

'''
Part 4: Comparing models with (Monte Carlo) cross-validation
'''
# Making a holder for the top 2 models' metrics
pca_mx = pd.DataFrame(np.zeros([10, 14]),
                      columns=pca_stats.columns)
nn_mx = pd.DataFrame(np.zeros([10, 14]),
                     columns=nn_stats.columns)

# Setting some parameters for the experiment
n_iter = 10
seeds = np.random.randint(0, 1e6, n_iter)

# Running the loop with code copied-and-pasted from earlier. Be sure
# to keep your object names straight, though!
for i, seed in enumerate(seeds):
    # Splitting the data
    train, test = train_test_split()
    
    # Training the SVM with PCA features, getting predictions on the test set,
    # and writing the binary metrics to the pca_mx data frame
    loop_svm =
    
    # Training the NN with binary features
    loop_nn = 
    
    # Generating and thresholding the probabilities, and writing the binary
    # metrics to the nn_mx data frame
    loop_nn_preds = loop_nn.predict()
    prev = np.sum(tb[train]) / len(tb[train])

# Running some statistical tests to compare the models; better option is
# to export _mx to CSV and analyze in stats software, e.g. R, since 
# scipy.stats.wilcoxon can't do exact tests.
f1_diff = 
diff_diff = 
f1_test = sp.stats.wilcoxon()
diff_test = sp.stats.wilcoxon()
