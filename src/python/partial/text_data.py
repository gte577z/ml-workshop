'''
This script will show you how to do some different things with text data.
'''
import numpy as np
import pandas as pd

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split
from sklearn.metrics import brier_score_loss

from tools import *

'''
Part 1: Data import and text preprocessing
'''
# Importing the data
outdir = ''
records = pd.read_csv()

# Making a count-valued document-term matrix with bigram features and a 
# minimum document frequency of 10
vec = CountVectorizer()
count_mat = vec.fit_transform()

# Transforming the matrix with TF-IDF weights
trans = TfidfTransformer()
tfidf_mat = trans.fit_transform()

# Pulling out the vocabulary
vocab = vec.vocabulary_

# Making a target variable for flu; use '123' as the pattern to match
y = np.array()

# Prevalence calculation
flu_prev = np.sum(y) / len(y)

# Adding the target back in to the original dataset
records['asthma'] = y

'''
Part 2: Training the models
'''
# Splitting the data
seed = 10221983
n_range = range(len(records))
train, not_train = train_test_split(n_range,
                                    stratify=y,
                                    test_size=0.4,
                                    random_state=seed)
val, test = train_test_split()

# Training and scoring the models. For the RF, use 500 trees, and train it
# on the TF-IDF features. For the MNB classifier, use the count features.
rf = RandomForestClassifier()
rf.fit()
rf_acc = rf.score()

mnb = MultinomialNB()
mnb.fit()
mnb_acc = 

'''
Part 3: Tuning the models
'''
# Getting predicted probabilities for the validation data
rf_val_probs = rf.predict_proba()[:, 1]
mnb_val_probs = mnb.predict_proba()[:, 1]

# Selecting thresholds for classification; use the cutoff with the best F1
# value for the F1 cut, and the cutoff with the smallest difference in counts
# for the 'bc' cut.
rf_gm = grid_metrics()
rf_f1_cut = rf_gm.cutoff[np.argmax()]
rf_bc_cut = rf_gm.cutoff[]

# Do the same for the MNB classifier.
mnb_gm = 
mnb_f1_cut = 
mnb_bc_cut = 

# Getting predicted probabilities for the test data
rf_test_probs = 
mnb_test_probs = 

# Thresholding and calculating classification metrics
rf_f1_preds = threshold(rf_test_probs, rf_f1_cut)
rf_bc_preds = threshold()
rf_stats = pd.concat([binary_metrics(),
                      binary_metrics()],
                     axis=0)

mnb_f1_preds = 
mnb_bc_preds = 
mnb_stats = 

# Calculating Brier scores as an alternative measure
rf_brier = brier_score_loss()
mnb_brier = brier_score_loss()

'''
Part 4: Interpreting the results
'''
# Getting the top 100 most "important" words for each classifier
rf_features = np.argsort(rf.feature_importances_)[::-1]
rf_topn = rf_features[]
rf_top_words = np.array([get_key(value, vocab) for value in rf_topn])

mnb_features = np.argsort(mnb.coef_.flatten())[::-1]
mnb_topn = 
mnb_top_words = 

# Figuring out where the models agree and disagree
models_agree = np.intersect1d(rf_top_words, mnb_top_words)
rf_not_mnb = rf_top_words[np.where([])[0]]
mnb_not_rf = mnb_top_words[np.where([])[0]
