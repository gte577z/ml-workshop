'''
This script will show you how to do some different things with text data.
'''
import numpy as np
import pandas as pd

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split
from sklearn.metrics import brier_score_loss

from tools import *

'''
Part 1: Data import and text preprocessing
'''

'''
Part 2: Training the models
'''

'''
Part 3: Tuning the models
'''

'''
Part 4: Interpreting the results
'''
