library(e1071)
library(keras)

# Importing the data
outdir <- ''
setwd(outdir)
records <- read.csv('tb_bin.csv')

# Separating MTB from the rest of the columns
tb <- 
features <- 
feat.mat <- as.matrix(features)
n.features <- 
n.records <- 

# Calculating prevalence of TB
tb.prev <- 

# Splitting our data into training and test sets
train <- read.csv('tb_train.csv')[, 1]
test <- read.csv('tb_test.csv')[, 1]

################################################################
# Part 1: Dimension reduction
################################################################
# Running PCA and keeping only the first 64 principle components
pca <- prcomp()
pca.features <- 

# Writing files to disk for visualization
metadata <- records[, c('mtb', 'CHOOSE SOME OTHER COLUMNS')]
write.table(metadata, file=paste0(outdir, 'metadata.tsv'), sep='\t',
            row.names=F)
write.table(features, file=paste0(outdir, 'features.tsv'), sep='\t',
            col.names=F, row.names=F)
write.table(pca.features,file=paste0(outdir, 'pca_features.tsv'),
            sep='\t', col.names=F, row.names=F)

################################################################
# Part 2: Using an SVM to predict TB from the 3 feature sets
################################################################
# Fitting the SVM, getting the test predictions, and calculating
# our binary classification metrics. Use a linear kernel and 
# 'C-classification' type for the svm.
svm.bin  <- svm()
svm.preds <- as.numeric(predict()) - 1
svm.stats <-  binary.metrics()

# And now with the PCA features
svm.pca <- 
pca.preds <- 
pca.stats <- 

################################################################
# Part 3: Using a network to do both tasks at once
################################################################
# Building the model and setting up some callbacks for training 
nn <- DNN()
stop <- callback_early_stopping(monitor='loss', patience=30)
board <- callback_tensorboard(log_dir='logs/', write_graph=T)

# Compiling the model. Use Adam for optimization and binary 
# cross-entropy for the loss function.
nn %>% compile()

# Fitting the model to the training data. Run the procedure
# for 500 epochs, and remember to add the callbacks.
nn %>% fit()

# Generating class predictions for the test data and getting the stats.
# Use the prevalence of flu in the training data as the cutoff for 
# the predicted probabilities.
nn.preds <- predict()
nn.stats <- binary.metrics()

# Using grid.metrics to pick a better threshold
nn.gm <- grid.metrics()
best.f1 <- 
f1.cut <- 

################################################################
# Part 4: Comparing the top two models with cross-validation
################################################################
# Setting parameters for the experiment
n.iter <- 10
seeds <- sample(1:1e6, n.iter)

# Making holders for the results
pca.mx <- data.frame(matrix(NA, nrow=n.iter, ncol=14))
nn.mx <- data.frame(matrix(NA, nrow=n.iter, ncol=14))
colnames(pca.mx) <- colnames(pca.stats)
colnames(nn.mx) <- colnames(nn.stats)

# Running the experiment
for (i in 1:n.iter){
  print(paste0('Running iteration number', as.character(i)))
  # Splitting the data
  seed <- seeds[i]
  splits <- train.test.split(features, test.size=0.25,
                             stratify=tb)
  train <- splits$train
  test <- splits$test
  
  # Running the SVM with PCA features
  loop.svm <- svm()
  loop.svm.preds <- 
  pca.mx[i, ] <- binary.metrics()
  
  # Running the neural network
  loop.nn <- DNN()
  stop <- callback_early_stopping()
  loop.nn %>% compile()
  loop.nn %>% fit()
  
  # Generating class predictions and getting the stats
  loop.nn.preds <- predict()
  prev <- 
  nn.mx[i, ] <- binary.metrics()
}

# Comparing the models on F1. Use the paired, exact version of
# the Wilcoxon signed-rank test.
f1.test <- 

