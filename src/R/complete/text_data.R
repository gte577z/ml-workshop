library(ranger)
library(e1071)
library(naivebayes)

################################################################
#Part 1: Data import and cc preprocessing
################################################################
# Importing the data
outdir <- '~/code/ml-workshop-dev/dat/'
setwd(outdir)
records <- read.csv('text.csv')
cc <- as.character(records$text)
dx <- as.character(records$diagnosis)

# Making the count matrix
counts <- doc.term.mat(cc)
count.mat <- counts$counts
vocab <- counts$vocab

# Turning the matrix into a data frame
count.df <- as.data.frame(count.mat)
colnames(count.df) <- vocab

# Transforming the counts to TF-IDF and binary values
counts.transformed <- tfidf.transform(count.df)
tfidf.df <- counts.transformed$tfidf
bin.df <- counts.transformed$bin
  
# Making a classification target and a full DF to use with ranger; 
# code 123 is the CCS code for asthma
y <- grepl('123', dx)
y.num <- as.numeric(y)
full.df <- tfidf.df
full.df$y <- as.factor(y)

# Calculating prevalence
flu.prev = sum(y == T) / length(y)

# Adding back to the original data frame
records$flu <- y

################################################################
# Part 2: Training the models
################################################################
# Importing datasplit indices
train <- read.csv('text_train.csv')[, 1]
val <- read.csv('text_val.csv')[, 1]
test <- read.csv('text_test.csv')[, 1]

# Fitting the random forest
rf.mod <- ranger(dependent.variable.name='y', data=full.df[train, ], 
                 probability=T, importance='impurity')
rf.probs <- predict(rf.mod, tfidf.df[test, ])$predictions[, 2]
rf.preds <- threshold(rf.probs)
rf.stats <- binary.metrics(y.num[test], rf.preds)

# Fitting the MNB classifier
mnb.mod <- naive_bayes(x=bin.df[train, ], y=y[train], laplace=1)
mnb.preds <- predict(mnb.mod, bin.df[test, ], type=c('prob'))

################################################################
# Part 3: Tuning the models
################################################################
# Getting predicted probabilities for the validation data
rf.val.probs <- predict(rf.mod, tfidf.df[val, ])$predictions[, 2]

# Finding optimal cutpoints for the 2 metrics
rf.gm <- grid.metrics(y.num[val], rf.val.probs, max=0.1, step=0.001)
rf.f1.cut <- rf.gm$cutoff[which.max(rf.gm$f1)]
rf.bc.cut <- rf.gm$cutoff[which.max(rf.gm$mcnemar)]

# Using the val cutoffs to get the test predictions
rf.f1.preds <- threshold(rf.probs, rf.f1.cut)
rf.bc.preds <- threshold(rf.probs, rf.bc.cut)

# Getting the final test statistics
rf.f1.stats <- binary.metrics(y.num[test], rf.f1.preds)
rf.bc.stats <- binary.metrics(y.num[test], rf.bc.preds)

# Figuring out which words had the highest importance
feature.imps <- rf.mod$variable.importance
top.features <- sort(feature.imps, decreasing=T)[1:100]
top.words <- names(top.features)
