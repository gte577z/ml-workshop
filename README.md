# ML Workshop
Code from the DataHub's "Machine Learning for Public Health: A Workshop" webinar.

## Introduction
Welcome to the workshop! Our goal will be to cover some basic machine learning
tasks in two programming languages: R, and Python. Using two datasets--one
with binary variables, and one with free-text--we will try our hands at
data preprocessing, data visualization, dimensionality reduction, and several
classification tasks. To accomplish these tasks, we will use a variety of
methods, including principal component analysis, random forests, support vector 
machines, and a deep (though not very complex) feedforward neural network. By 
the end of the workshop, you should have some working code, along with a basic
understanding of the methods you implemented, both of which will hopefully
serve you well on your own projects.

### Repository structure
Data is in [data](data), documentation is in [docs](docs), and code is in 
[src](src). 

### Technical requirements
Before diving into the code, please make sure you have the following packages
and their dependencies installed:

Python 3.x: [scikit-learn](http://scikit-learn.org/stable/), [NumPy](http://www.numpy.org/),
 [pandas](https://pandas.pydata.org/), [Matplotlib](https://matplotlib.org), 
 [TensorFlow](https://www.tensorflow.org/), and [Keras](https://keras.io/).

R: [ranger](https://cran.r-project.org/web/packages/ranger/index.html), 
[e1071](https://cran.r-project.org/web/packages/e1071/index.html), [Keras](https://keras.rstudio.com/), 
[text2vec](http://text2vec.org), and [naivebayes](https://cran.r-project.org/web/packages/naivebayes/index.html).

You won't need an integrated development environment (IDE) to run the code, but using
one might make it easier to manage your project and keep things neat. For R, we recommend 
[RStudio](https://www.rstudio.com/products/rstudio/download/#download), and for Python,
we recommend either [Atom](https://atom.io) or 
[IDLE](https://docs.python.org/3/library/idle.html). Please have either Chrome 
or Firefox installed, as well.

#### Disclaimer
Although we can help you debug your code, we will not be able to provide any
more substantial tech support during the workshop. Please make sure that these
packages are not only installed before showing up, but that they are also 
working, especially if you're using R.

## Datasets
We'll be working with two datasets for this workshop: `tb.csv`, 
a clinical dataset from a study of identifying TB in peopl with HIV; 
and `text.csv`, an emergency department dataset (ED) with chief complaints and 
their corresponding discharge diagnoses. We'll do a quick overview of both datasets
below, but because the TB/HIV dataset is a little complex, we've also added its 
data dictionary to [doc](doc) so you can find more information about its 
variables and structure.

### ID-TB/HIV clinical data
These data were collected as part of a CDC study to develop a screening algorithm 
for TB in people with HIV. The original paper was published in the New
England Journal of Medicine (read the 
[article](https://doi.org/10.1056/NEJMoa0907488) here),
and the algorithm later influenced the development of the WHO's own set of
screening guidelines. The dataset comprises about 200 variables collected from
around 2,000 patients, and the variables capture many different types of 
clinical information. Here are some examples:

    1. Binary: TB status, sex, hospitalization, and presence of symptoms
    2. Nominal: Country, culture results, and behavioral risk factors
    3. Ordered: Karnofsky performance status, smear results, and severity of symptoms
    4. Continuous: Age, BMI, CD4 cell count, and lymphocyte count
    
For the workshop, we'll limit our analysis to the binary variables, but feel 
free to explore the others on your own--they're pretty interesting.

### Emergency department (ED) visit records
The original version of this dataset was provided by the New York City Department
of Health and Mental Hygiene. It spanned about 1.5 years (2016-2017) and included
nearly 6 million patient-level records. Here, we've trimmed the size of the 
dataset down to about 40,000 records, and we had a bot replace the original chief 
complaints with synthetic chief complaints generated from other variables in the
records (for more information about the generation process, see 
[this article](https://doi.org/10.1038/s41746-018-0070-0) in npj Digital 
Medicine). We also binned the original ICD-9/10 discharge diagnoses into [Clinical
Classification Software](https://www.hcup-us.ahrq.gov/toolssoftware/ccs/ccs.jsp)
(CCS) code groups. The grouping system was developed by HCUP, and it reduces 
the possible number of unique discharge diagnoses from about 70,000 to about 
275, making it much easier to train predictive models that work. Here are some 
example chief complaint/diagnosis pairs from the dataset:

    1. 'pt admits to drinking alcohol': CCS 660 Alcohol-related disorders
    2. 'back pain x 1 week': CCS 159 Urinary tract infections; CCS 211 Other connective tissue disease
    3. 'abdominal pain and diarrhea': CCS 154 Noninfectious gastroenteritis
    4. 'encounter for removal of sutures': CCS 257 Other aftercare
    5. 'dizziness': CCS 250 Nausea and vomiting; CCS 93 Conditions associated with dizziness or vertigo

Because of the way they're generated, the synthetic chief complaints tend to be 
more "canonical" than their authentic counterparts, i.e., they show less 
linguistic variation, also making them easier to classify with a model.

## Tasks
### Dimensionality reduction
Our first task will be dimensionality reduction, and we'll use the TB dataset as
our example. For our method, we'll use 
[principal component analysis](https://en.wikipedia.org/wiki/Principal_component_analysis),
which transforms the original (possible correlated) binary variables into a set
of linearly-uncorrelated variables (i.e., the principal components). We'll then
use t-distributed stochastic neighbor embeddings, or 
[t-SNE](https://lvdmaaten.github.io/tsne/), to visualize the compressed features
and look for interesting clusters. 

### Prediction from binary variables
Our next task will be to predict which patients in the TB dataset had positive
TB cultures based on their clinical information. For our methods, we'll try a
[support vector machine](https://en.wikipedia.org/wiki/Support_vector_machine) (SVM) 
with both the raw and the PCA features, and we'll also use a 
[deep neural network](https://en.wikipedia.org/wiki/Deep_learning#Deep_neural_networks). 
Since the neural network can learn dimension-reduced feature representations 
on its own, we'll feed it the raw features and see how it fares. 

### Prediction from text
Our final task will be to predict the discharge diagnoses in the ED dataset
from their corresponding chief complaints. This is similar to the kind of prediction
tasks that are central to syndromic surveillance, and it's a good way to get 
familiar with machine learning methods for text classification. Because of its
strong performance and good interpretability, we'll use the 
[random forest](https://en.wikipedia.org/wiki/Random_forest) as our classifier,
and if we have time, we'll talk about how you might tackle the same problem with 
[recurrent neural networks](http://karpathy.github.io/2015/05/21/rnn-effectiveness/).

## Goals
Our main goal for the workshop is to give you hands-on experience with ML while
also having fun! That being said, by the end of the workshop you should be able 
to do the following:

    1. Load, preprocess, and explore data;
    2. Choose appropriate models for your task;
    3. Train, tune, and evaluate models;
    4. Compare competing models' performance; and
    4. Write sustainable code in R and/or Python.
    
We will not have time to cover other ipmortant topics in applied machine
learning, like the basic principles of object-oriented design or high-
performance computing, but what we *do* cover will hopefully give you enough of a
foundation to explore them on your own.
